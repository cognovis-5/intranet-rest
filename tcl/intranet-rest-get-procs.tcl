# /packages/intranet-rest/tcl/intranet-rest-get-procs.tcl
#
# Copyright (C) 2009 ]project-open[
#
# All rights reserved. Please check
# http://www.project-open.com/license/ for details.

ad_library {
    REST Web Service Component Library
    @author frank.bergmann@project-open.com
}


ad_proc -public im_rest_get_object_type {
    { -columns ""}
    { -deref_p 0 }
    { -query "" }
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
    { -limit ""}
    { -start ""}
} {
    Handler for GET rest calls on a whole object type -
    mapped to queries on the specified object type

    @param columns json_list List of columns to return
    @param deref_p boolean Should we dereference certain columns?
    @param query string Additional where clause for the SQL query
    @param limit number Appends pagination information to a SQL statement depending on URL parameters
    @param start number Start for the limit of the pagination
} {
    ns_log Debug "im_rest_get_object_type: format=$format, rest_user_id=$rest_user_id, rest_otype=$rest_otype, rest_oid=$rest_oid, query_hash=$query_hash_pairs"
    set org_format $format
    array set query_hash $query_hash_pairs
    set rest_otype_id [util_memoize [list db_string otype_id "select object_type_id from im_rest_object_types where object_type = '$rest_otype'" -default 0]]
                    
    # Check if the deref_p parameter was set
    array set query_hash $query_hash_pairs

    im_security_alert_check_integer -location "im_rest_get_object: deref_p" -value $deref_p

    set base_url "[im_rest_system_url]/intranet-rest"

    # -------------------------------------------------------
    # Get some more information about the current object type
    set otype_info [util_memoize [list db_list_of_lists rest_otype_info "select table_name, id_column from acs_object_types where object_type = '$rest_otype'"]]

    set table_name [lindex $otype_info 0 0]
    set id_column [lindex $otype_info 0 1]
    if {"" == $table_name} {
	return [im_rest_error -format $org_format -http_status 500 -message "Invalid DynField configuration: Object type '$rest_otype' doesn't have a table_name specified in table acs_object_types."]
    }
    # Deal with ugly situation that usre_id is defined multiple times for object_type=user
    if {"users" == $table_name} { set id_column "person_id" }
    
    if {$rest_oid eq ""} {
        # Set the rest_oid to the id_column in 
        # case the ID column was provided as a filter
        if {[info exists query_hash($id_column)]} {
            set rest_oid $query_hash($id_column)
        }
    } 


    # -------------------------------------------------------
    # Check if there is a where clause specified in the URL
    # and validate the clause.
    set where_clause_list [list]
    set where_clause_unchecked_list [list]
    if {$query ne ""} {
        lappend where_clause_list $query
    }

    # -------------------------------------------------------
    # Check if there are "valid_vars" specified in the HTTP header
    # and add these vars to the SQL clause
    set valid_vars [im_rest_object_type_columns -deref_p $deref_p -rest_otype $rest_otype -data_type_arr data_types]

    foreach v $valid_vars {
	    if {[info exists query_hash($v)]} {

            set value $query_hash($v)

            # Check for special values
            switch $value {
                "NULL" {
                    lappend where_clause_list "$v is null"
                }
                "NOT NULL" {
                    lappend where_clause_list "$v is not null"
                }
                default {
                    lappend where_clause_list "$v = $value"
                }
            }
        }
    }

    # -------------------------------------------------------
    # Check if there was a rest_oid provided as part of the URL
    # for example /im_project/8799. In this case add the oid to
    # the query.
    # rest_oid was already security checked to be an integer.
    if {"" != $rest_oid && 0 != $rest_oid} {
	    lappend where_clause_list "$id_column=$rest_oid"
    }

    # -------------------------------------------------------
    # Transform the database table to deal with exceptions
    #
    switch $rest_otype {
        user - person - party {
            set table_name "(
            select	*
            from	users u, parties pa, persons pe
            where	u.user_id = pa.party_id and u.user_id = pe.person_id and
                u.user_id in (
                    SELECT  o.object_id
                    FROM    acs_objects o,
                            group_member_map m,
                            membership_rels mr
                    WHERE   m.member_id = o.object_id AND
                            m.group_id = acs__magic_object_id('registered_users'::character varying) AND
                            m.rel_id = mr.rel_id AND
                            m.container_id = m.group_id AND
                            m.rel_type::text = 'membership_rel'::text AND
                            mr.member_state = 'approved'
                )
            )"
        }
        file_storage_object {
            # file storage object needs additional security
            lappend where_clause_unchecked_list "'t' = acs_permission__permission_p(rest_oid, $rest_user_id, 'read')"
        }
        im_ticket {
            # Testing per-ticket permissions
            set read_sql [im_ticket_permission_read_sql -user_id $rest_user_id]
            lappend where_clause_unchecked_list "rest_oid in ($read_sql)"
        }
    }

    # Check that the where_clause elements are valid SQL statements
    foreach where_clause $where_clause_list {
	    set valid_sql_where [im_rest_valid_sql -string $where_clause -variables $valid_vars]
	    if {!$valid_sql_where} {
	        return [im_rest_error -format $org_format -http_status 403 -message "The specified query is not a valid SQL where clause: '$where_clause'"]
	    }
    }

    # Build the complete where clause
    set where_clause_list [concat $where_clause_list $where_clause_unchecked_list]
    if {[llength $where_clause_list]>0} {
        set where_sql "where [join $where_clause_list " and\n\t\t"]"
    } else {
        set where_sql ""
    }
    
    # -------------------------------------------------------
    # Select SQL: Pull out objects where the acs_objects.object_type 
    # is correct AND the object exists in the object type's primary table.
    # This way we avoid "dangling objects" in acs_objects and sub-types.
    set sql [im_rest_object_type_select_sql -deref_p $deref_p -rest_otype $rest_otype -no_where_clause_p 1]
    append sql "
	where	o.object_type in ('[join [im_rest_object_type_subtypes -rest_otype $rest_otype] "','"]') and
		o.object_id in (
			select  t.$id_column
			from    $table_name t
		)\
    "

    # Add $where_clause to the outside of the SQL in order to
    # avoid ambiguities of duplicate columns like "rel_id"
    set sql "
	select	*
	from	($sql
		) t
	$where_sql
    "
    
    # Append sorting "ORDER BY" clause to the sql.
    append sql [im_rest_object_type_order_sql -query_hash_pairs $query_hash_pairs]

    # Append pagination "LIMIT $limit OFFSET $start" to the sql.
    append sql [im_rest_object_type_pagination_sql -limit $limit -start $start]

	# -------------------------------------------------------
	# Permissions
    # -------------------------------------------------------

    # Check for generic permissions to read all objects of this type
    set rest_otype_read_all_p [im_object_permission -object_id $rest_otype_id -user_id $rest_user_id -privilege "read"]

    if {!$rest_otype_read_all_p} {
        # There are "view_..._all" permissions allowing a user to see all objects:
        switch $rest_otype {
            bt_bug		{ }
            im_company		{ set rest_otype_read_all_p [im_permission $rest_user_id "view_companies_all"] }
            im_cost		{ set rest_otype_read_all_p [im_permission $rest_user_id "view_finance"] }
            im_conf_item	{ set rest_otype_read_all_p [im_permission $rest_user_id "view_conf_items_all"] }
            im_invoices		{ set rest_otype_read_all_p [im_permission $rest_user_id "view_finance"] }
            im_project		{ set rest_otype_read_all_p [im_permission $rest_user_id "view_projects_all"] }
            im_user_absence	{ set rest_otype_read_all_p [im_permission $rest_user_id "view_absences_all"] }
            im_office		{ set rest_otype_read_all_p [im_permission $rest_user_id "view_offices_all"] }
            im_profile		{ set rest_otype_read_all_p 1 }
            im_ticket		{ set rest_otype_read_all_p [im_permission $rest_user_id "view_tickets_all"] }
            im_timesheet_task	{ set rest_otype_read_all_p [im_permission $rest_user_id "view_timesheet_tasks_all"] }
            im_timesheet_invoices { set rest_otype_read_all_p [im_permission $rest_user_id "view_finance"] }
            im_trans_invoices	{ set rest_otype_read_all_p [im_permission $rest_user_id "view_finance"] }
            im_translation_task	{ }
            user		{ }
            default { 
                # No read permissions? 
                # Well, no object type except the ones above has a custom procedure,
                # so we can deny access here:
                return [im_rest_error -format $org_format -http_status 403 -message "User #$rest_user_id does not have read access to objects of type $rest_otype"]
            }
        }
    }

    set result ""
    set user_id $rest_user_id

    # Check permission procedure
    set permission_proc ""
    if {!$rest_otype_read_all_p} {
        # Check if there is a special proc for the object_tpye
        if { [llength [info commands ${rest_otype}_permissions]]} {
            set permission_proc "${rest_otype}_permissions"
        }
    }

    # Only return those vars which are valid and which were requested
    if {$columns ne ""} {
        set valid_vars_orig $valid_vars
        set valid_vars [list]
        foreach valid_var $valid_vars_orig {
            if {[lsearch $columns $valid_var]>-1} {
                lappend valid_vars $valid_var
            }
        }
        
    }

    set return_objects [im_rest_objects -sql $sql -valid_vars $valid_vars -rest_user_id $rest_user_id -data_type_arr data_types -permission_proc $permission_proc -format $org_format -rest_oid $rest_oid -rest_otype $rest_otype]
    switch $org_format {
        html {
            set page_title "object_type: $rest_otype"
            im_rest_doc_return 200 "text/html" "
            [im_header $page_title [im_rest_header_extra_stuff]][im_navbar]<table>
            <tr class=rowtitle><td class=rowtitle>object_id</td><td class=rowtitle>Link</td></tr>$return_objects
            </table>[im_footer]
            " 
        }
        json {
            if {$return_objects ne 0} {
                # If im_rest_objects returns a 0, then we have an error
                set json_list $return_objects
                set result "{\"success\": true,\n\"total\": [llength $json_list],\n\"message\": \"im_rest_get_object_type: Data loaded\",\n\"data\": \[\n[join $json_list ","]\n\]\n}"
                im_rest_doc_return 200 "application/json" $result
            }
            return
        }
        default {
            ad_return_complaint 1 "im_rest_get_object_type: Invalid format5: '$org_format'"
            return
        }
    }
}


#---------------------------------------------------------------
# Non ACS-Object "objects"
#---------------------------------------------------------------

ad_proc -private im_rest_get_im_invoice_item {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest calls on invoice items.
} {
    ns_log Notice "im_rest_get_invoice_items: format=$format, rest_user_id=$rest_user_id, rest_otype=$rest_otype, query_hash=$query_hash_pairs"
    
    array set query_hash $query_hash_pairs
    if {"" != $rest_oid} { set query_hash(item_id) $rest_oid }
    
    set base_url "[im_rest_system_url]/intranet-rest"
    set rest_otype_id [util_memoize [list db_string otype_id "select object_type_id from im_rest_object_types where object_type = 'im_invoice'" -default 0]]
    set rest_otype_read_all_p [im_permission $rest_user_id "view_finance"]

    # -------------------------------------------------------
    # Check if there is a where clause specified in the URL and validate the clause.
    set where_clause ""
    if {[info exists query_hash(query)]} { set where_clause $query_hash(query)}
    # Determine the list of valid columns for the object type
    set valid_vars {item_id item_name project_id invoice_id item_units item_uom_id price_per_unit currency sort_order item_type_id item_status_id description item_material_id}
    # Check that the query is a valid SQL where clause
    set valid_sql_where [im_rest_valid_sql -string $where_clause -variables $valid_vars]
    if {!$valid_sql_where} {
	return [im_rest_error -format $format -http_status 403 -message "The specified query is not a valid SQL where clause: '$where_clause'"]
    }
    if {"" != $where_clause} { set where_clause "and $where_clause" }

    # Select SQL: Pull out invoice_items.
    set sql "
	select	ii.item_id as rest_oid,
		ii.item_name as object_name,
		ii.*
	from	im_invoice_items ii
	where	1=1
		$where_clause
    "

    # Append pagination "LIMIT $limit OFFSET $start" to the sql.
    set unlimited_sql $sql
    append sql [im_rest_object_type_pagination_sql -query_hash_pairs $query_hash_pairs]

    set result ""
    set obj_ctr 0
    db_foreach objects $sql {

	# Check permissions
	set read_p $rest_otype_read_all_p
	if {!$read_p} { im_invoice_permissions $rest_user_id $invoice_id view_p read_p write_p admin_p }
	if {!$read_p} { continue }

	set url "$base_url/$rest_otype/$rest_oid"
	switch $format {
	    html { 
		append result "<tr>
			<td>$rest_oid</td>
			<td><a href=\"$url?format=html\">$object_name</a>
		</tr>\n" 
	    }
	    json {
		set komma ",\n"
		if {0 == $obj_ctr} { set komma "" }
		set dereferenced_result ""
		foreach v $valid_vars {
			eval "set a $$v"
			regsub -all {\n} $a {\n} a
			regsub -all {\r} $a {} a
			append dereferenced_result ", \"$v\": \"[im_quotejson $a]\""
		}
		append result "$komma{\"id\": \"$rest_oid\", \"object_name\": \"[im_quotejson $object_name]\"$dereferenced_result}" 
	    }
	    default {}
	}
	incr obj_ctr
    }

    switch $format {
	html { 
	    set page_title "object_type: $rest_otype"
	    im_rest_doc_return 200 "text/html" "
		[im_header $page_title [im_rest_header_extra_stuff]][im_navbar]<table>
		<tr class=rowtitle><td class=rowtitle>object_id</td><td class=rowtitle>Link</td></tr>$result
		</table>[im_footer]
	    "
	}
	json {
	    set result "{\"success\": true,\n\"total\": $obj_ctr,\n\"message\": \"im_rest_get_im_invoice_item: Data loaded\",\n\"data\": \[\n$result\n\]\n}"
	    im_rest_doc_return 200 "application/json" $result
	    return
	}
    }

    return
}


ad_proc -private im_rest_get_im_hour {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest calls on timesheet hours
} {
    ns_log Notice "im_rest_get_im_hour: format=$format, rest_user_id=$rest_user_id, rest_otype=$rest_otype, rest_oid=$rest_oid, query_hash=$query_hash_pairs"

    array set query_hash $query_hash_pairs
    if {"" != $rest_oid} { set query_hash(hour_id) $rest_oid }
    set base_url "[im_rest_system_url]/intranet-rest"

    # Permissions:
    # A user can normally read only his own hours,
    # unless he's got the view_hours_all privilege or explicitely 
    # the perms on the im_hour object type
    set rest_otype_id [util_memoize [list db_string otype_id "select object_type_id from im_rest_object_types where object_type = 'im_hour'" -default 0]]
    set rest_otype_read_all_p [im_object_permission -object_id $rest_otype_id -user_id $rest_user_id -privilege "read"]
    if {[im_permission $rest_user_id "view_hours_all"]} { set rest_otype_read_all_p 1 }

    set owner_perm_sql "and h.user_id = :rest_user_id"
    if {$rest_otype_read_all_p} { set owner_perm_sql "" }

    # -------------------------------------------------------
    # Check if there is a where clause specified in the URL and validate the clause.
    set where_clause ""
    if {[info exists query_hash(query)]} { set where_clause $query_hash(query)}

    # Determine the list of valid columns for the object type
    set valid_vars {hour_id user_id project_id day hours days note internal_note cost_id conf_object_id invoice_id material_id}



    # -------------------------------------------------------
    # Check if there are "valid_vars" specified in the HTTP header
    # and add these vars to the SQL clause
    set where_clause_list [list]
    foreach v $valid_vars {
        if {[info exists query_hash($v)]} { lappend where_clause_list "$v=$query_hash($v)" }
    }
    if {"" != $where_clause && [llength $where_clause_list] > 0} { append where_clause " and " }
    append where_clause [join $where_clause_list " and "]

    # Check that the query is a valid SQL where clause
    set valid_sql_where [im_rest_valid_sql -string $where_clause -variables $valid_vars]
    if {!$valid_sql_where} {
	return [im_rest_error -format $format -http_status 403 -message "The specified query is not a valid SQL where clause: '$where_clause'"]
    }
    if {"" != $where_clause} { set where_clause "and $where_clause" }

    # Select SQL: Pull out hours.
    set sql "
	select	h.hour_id as rest_oid,
		'(' || im_name_from_user_id(user_id) || ', ' || 
			im_project_name_from_id(h.project_id) || 
			day::date || ', ' || ' - ' || 
			h.hours || ')' as object_name,
		h.*
	from	im_hours h
	where	1=1
		$owner_perm_sql
		$where_clause
    "

    # Append pagination "LIMIT $limit OFFSET $start" to the sql.
    set unlimited_sql $sql
    append sql [im_rest_object_type_pagination_sql -query_hash_pairs $query_hash_pairs]

    set value ""
    set result ""
    set obj_ctr 0
    db_foreach objects $sql {

	# Check permissions
	set read_p $rest_otype_read_all_p
	if {!$read_p} { continue }

	set url "$base_url/$rest_otype/$rest_oid"
	switch $format {
	    html { 
		append result "<tr>
			<td>$rest_oid</td>
			<td><a href=\"$url?format=html\">$object_name</a>
		</tr>\n"
	    }
	    json {
		set komma ",\n"
		if {0 == $obj_ctr} { set komma "" }
		set dereferenced_result ""
		foreach v $valid_vars {
			eval "set a $$v"
			regsub -all {\n} $a {\n} a
			regsub -all {\r} $a {} a
			append dereferenced_result ", \"$v\": \"[im_quotejson $a]\""
		}
		append result "$komma{\"id\": \"$rest_oid\", \"object_name\": \"[im_quotejson $object_name]\"$dereferenced_result}" 
	    }
	    default {}
	}
	incr obj_ctr
    }
	
    switch $format {
	html { 
	    set page_title "object_type: $rest_otype"
	    im_rest_doc_return 200 "text/html" "
		[im_header $page_title [im_rest_header_extra_stuff]][im_navbar]<table>
		<tr class=rowtitle><td class=rowtitle>object_id</td><td class=rowtitle>Link</td></tr>$result
		</table>[im_footer]
	    "
	}
	json {  
	    set result "{\"success\": true,\n\"total\": $obj_ctr,\n\"message\": \"im_rest_get_im_hour: Data loaded\",\n\"data\": \[\n$result\n\]\n}"
	    im_rest_doc_return 200 "application/json" $result
	    return
	}
    }
    return
}



ad_proc -private im_rest_get_im_hour_interval {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest calls on timesheet hour intervals
} {
    ns_log Notice "im_rest_get_im_hour_interval: format=$format, rest_user_id=$rest_user_id, rest_otype=$rest_otype, rest_oid=$rest_oid, query_hash=$query_hash_pairs"

    array set query_hash $query_hash_pairs
    if {"" != $rest_oid} { set query_hash(interval_id) $rest_oid }
    set base_url "[im_rest_system_url]/intranet-rest"

    # Permissions:
    # A user can normally read only his own hours,
    # unless he's got the view_hours_all privilege or explicitely 
    # the perms on the im_hour_interval object type
    set rest_otype_id [util_memoize [list db_string otype_id "select object_type_id from im_rest_object_types where object_type = 'im_hour_interval'" -default 0]]
    set rest_otype_read_all_p [im_object_permission -object_id $rest_otype_id -user_id $rest_user_id -privilege "read"]
    if {[im_permission $rest_user_id "view_hours_all"]} { set rest_otype_read_all_p 1 }

    set owner_perm_sql "and h.user_id = :rest_user_id"
    if {$rest_otype_read_all_p} { set owner_perm_sql "" }

    # -------------------------------------------------------
    # Check if there is a where clause specified in the URL and validate the clause.
    set where_clause ""
    if {[info exists query_hash(query)]} { set where_clause $query_hash(query)}

    # Determine the list of valid columns for the object type
    set valid_vars {interval_id user_id project_id interval_start interval_end note internal_note activity_id material_id}


    # -------------------------------------------------------
    # Check if there are "valid_vars" specified in the HTTP header
    # and add these vars to the SQL clause
    set where_clause_list [list]
    foreach v $valid_vars {
        if {[info exists query_hash($v)]} { lappend where_clause_list "$v=$query_hash($v)" }
    }
    if {"" != $where_clause && [llength $where_clause_list] > 0} { append where_clause " and " }
    append where_clause [join $where_clause_list " and "]

    # Check that the query is a valid SQL where clause
    set valid_sql_where [im_rest_valid_sql -string $where_clause -variables $valid_vars]
    if {!$valid_sql_where} {
	return [im_rest_error -format $format -http_status 403 -message "The specified query is not a valid SQL where clause: '$where_clause'"]
    }
    if {"" != $where_clause} { set where_clause "and $where_clause" }



    # Select SQL: Pull out hours.
    set sql "
	select	h.interval_id as rest_oid,
		'(' || im_name_from_user_id(user_id) || ', ' || 
			im_project_name_from_id(h.project_id) || ', ' ||
			interval_start || ' - ' || interval_end || ')' as object_name,
		h.*
	from	im_hour_intervals h
	where	1=1
		$owner_perm_sql
		$where_clause
    "

    # Append pagination "LIMIT $limit OFFSET $start" to the sql.
    set unlimited_sql $sql
    append sql [im_rest_object_type_pagination_sql -query_hash_pairs $query_hash_pairs]

    set value ""
    set result ""
    set obj_ctr 0
    db_foreach objects $sql {

	# Check permissions
	set read_p $rest_otype_read_all_p
	if {!$read_p} { continue }

	set url "$base_url/$rest_otype/$rest_oid"
	switch $format {
	    html { append result "<tr><td>$rest_oid</td><td><a href=\"$url?format=html\">$object_name</a></tr>\n" }
	    json {
		set komma ",\n"
		if {0 == $obj_ctr} { set komma "" }
		set dereferenced_result ""
		foreach v $valid_vars {
			eval "set a $$v"
			regsub -all {\n} $a {\n} a
			regsub -all {\r} $a {} a
			append dereferenced_result ", \"$v\": \"[im_quotejson $a]\""
		}
		append result "$komma{\"id\": \"$rest_oid\", \"object_name\": \"[im_quotejson $object_name]\"$dereferenced_result}" 
	    }
	    default {}
	}
	incr obj_ctr
    }
	
    switch $format {
	html { 
	    set page_title "object_type: $rest_otype"
	    im_rest_doc_return 200 "text/html" "
		[im_header $page_title [im_rest_header_extra_stuff]][im_navbar]<table>
		<tr class=rowtitle><td class=rowtitle>object_id</td><td class=rowtitle>Link</td></tr>$result
		</table>[im_footer]
	    "
	}
	json {  
	    set result "{\"success\": true,\n\"message\": \"im_rest_get_im_hour_interval: Data loaded\",\n\"data\": \[\n$result\n\]\n}"
	    im_rest_doc_return 200 "application/json" $result
	    return
	}
    }
    return
}


ad_proc -private im_rest_get_im_timesheet_task_dependency {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest calls on task dependencies
} {
    ns_log Notice "im_rest_get_timesheet_task_dependencies: format=$format, rest_user_id=$rest_user_id, rest_otype=$rest_otype, query_hash=$query_hash_pairs"
    
    array set query_hash $query_hash_pairs
    if {"" != $rest_oid} { set query_hash(dependency_id) $rest_oid }
    set base_url "[im_rest_system_url]/intranet-rest"
    set rest_otype_id [util_memoize [list db_string otype_id "select object_type_id from im_rest_object_types where object_type = 'im_timesheet_task_dependency'" -default 0]]

    # "harmless" data-type, we can allow reading for everybody
    set rest_otype_read_all_p 1

    # -------------------------------------------------------
    # Check if there is a where clause specified in the URL and validate the clause.
    set where_clause ""
    if {[info exists query_hash(query)]} { set where_clause $query_hash(query)}
    # Determine the list of valid columns for the object type
    set valid_vars {dependency_id dependency_status_id dependency_type_id task_id_one task_id_two difference hardness_type_id}


    # -------------------------------------------------------
    # Check if there are "valid_vars" specified in the HTTP header
    # and add these vars to the SQL clause
    set where_clause_list [list]
    foreach v $valid_vars {
        if {[info exists query_hash($v)]} { lappend where_clause_list "$v=$query_hash($v)" }
    }
    if {"" != $where_clause && [llength $where_clause_list] > 0} { append where_clause " and " }
    append where_clause [join $where_clause_list " and "]

    # Check that the query is a valid SQL where clause
    set valid_sql_where [im_rest_valid_sql -string $where_clause -variables $valid_vars]
    if {!$valid_sql_where} {
	return [im_rest_error -format $format -http_status 403 -message "The specified query is not a valid SQL where clause: '$where_clause'"]
    }
    if {"" != $where_clause} { set where_clause "and $where_clause" }

    # Select SQL: Pull out timesheet_task_dependencies.
    set sql "
	select	d.*,
		d.dependency_id as rest_oid,
		'Task Dependency ' || task_id_one || ' - ' || task_id_two as object_name
	from	im_timesheet_task_dependencies d
	where	1=1
		$where_clause
    "

    # Append pagination "LIMIT $limit OFFSET $start" to the sql.
    set unlimited_sql $sql
    append sql [im_rest_object_type_pagination_sql -query_hash_pairs $query_hash_pairs]

    set result ""
    set obj_ctr 0
    db_foreach objects $sql {

	# Check permissions
	set read_p $rest_otype_read_all_p
	if {!$read_p} { continue }

	set url "$base_url/$rest_otype/$rest_oid"
	switch $format {
	    html { 
		append result "<tr>
			<td>$rest_oid</td>
			<td><a href=\"$url?format=html\">$object_name</a>
		</tr>\n" 
	    }
	    json {
		set komma ",\n"
		if {0 == $obj_ctr} { set komma "" }
		set dereferenced_result ""
		foreach v $valid_vars {
			eval "set a $$v"
			regsub -all {\n} $a {\n} a
			regsub -all {\r} $a {} a
			append dereferenced_result ", \"$v\": \"[im_quotejson $a]\""
		}
		append result "$komma{\"id\": \"$rest_oid\", \"object_name\": \"[im_quotejson $object_name]\"$dereferenced_result}" 
	    }
	    default {}
	}
	incr obj_ctr
    }

    switch $format {
	html { 
	    set page_title "object_type: $rest_otype"
	    im_rest_doc_return 200 "text/html" "
		[im_header $page_title [im_rest_header_extra_stuff]][im_navbar]<table>
		<tr class=rowtitle><td class=rowtitle>object_id</td><td class=rowtitle>Link</td></tr>$result
		</table>[im_footer]
	    "
	}
	json {
	    set result "{\"success\": true,\n\"total\": $obj_ctr,\n\"message\": \"im_rest_get_im_timesheet_task_dependency: Data loaded\",\n\"data\": \[\n$result\n\]\n}"
	    im_rest_doc_return 200 "application/json" $result
	    return
	}
    }

    return
}






ad_proc -private im_rest_get_im_category {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest calls on invoice items.
} {
    ns_log Notice "im_rest_get_im_category: format=$format, rest_user_id=$rest_user_id, rest_otype=$rest_otype, query_hash=$query_hash_pairs"
    array set query_hash $query_hash_pairs
    set base_url "[im_rest_system_url]/intranet-rest"

    if {"" != $rest_oid} { set query_hash(category_id) $rest_oid }
    
    
    set rest_otype_id [util_memoize [list db_string otype_id "select object_type_id from im_rest_object_types where object_type = 'im_category'" -default 0]]
    set rest_otype_read_all_p [im_object_permission -object_id $rest_otype_id -user_id $rest_user_id -privilege "read"]

    if {!$rest_otype_read_all_p} {
        return [im_rest_error -format $format -http_status 403 -message "User #$rest_user_id does not have read access to objects of type $rest_otype"]
    }

    # Get locate for translation
    set locale [lang::user::locale -user_id $rest_user_id]

    # -------------------------------------------------------
    # Valid variables to return for im_category
    set valid_vars {category_id tree_sortkey category category_translated category_description category_type category_gif enabled_p parent_only_p aux_int1 aux_int2 aux_string1 aux_string2 sort_order}
    
    foreach valid_var $valid_vars {
        switch $valid_var {
            category_id - aux_int1 - aux_int2 - sort_order {
                set data_types($valid_var) number
            }
            enabled_p - parent_only_p {
                set data_types($valid_var) boolean
            } 
            default {
                set data_types($valid_var) varchar
            }
        }
    }
     

    # -------------------------------------------------------
    # Check if there is a where clause specified in the URL and validate the clause.
    set where_clause ""
    if {[info exists query_hash(query)]} { set where_clause $query_hash(query)}

    # -------------------------------------------------------
    # Check if there are "valid_vars" specified in the HTTP header
    # and add these vars to the SQL clause
    set where_clause_list [list]
    foreach v $valid_vars {
        if {[info exists query_hash($v)]} { lappend where_clause_list "$v=$query_hash($v)" }
    }
    if {"" != $where_clause && [llength $where_clause_list] > 0} { append where_clause " and " }
    append where_clause [join $where_clause_list " and "]


    # Check that the query is a valid SQL where clause
    set valid_sql_where [im_rest_valid_sql -string $where_clause -variables $valid_vars]
    if {!$valid_sql_where} {
	return [im_rest_error -format $format -http_status 403 -message "The specified query is not a valid SQL where clause: '$where_clause'"]
    }
    if {"" != $where_clause} { set where_clause "and $where_clause" }

   if {[info exists query_hash(parent_category_id)]} {
   	   set parent_category_id $query_hash(parent_category_id)
       set subcategories [im_sub_categories $parent_category_id]
       append where_clause "and c.category_id in ([ns_dbquotelist $subcategories])"
   }

    # Select SQL: Pull out categories.
    set sql "
	select	c.category_id as rest_oid,
		c.category as object_name,
		im_category_path_to_category(c.category_id) as tree_sortkey,
		c.*
	from im_categories c
	where	(c.enabled_p is null OR c.enabled_p = 't')
		$where_clause
		
	order by category_id
    "

    # Append pagination "LIMIT $limit OFFSET $start" to the sql.
    set unlimited_sql $sql
    append sql [im_rest_object_type_pagination_sql -query_hash_pairs $query_hash_pairs]

    set value ""
    set result ""
    set obj_ctr 0

    set additional_var_procs [list [list {category_translated} {im_category_from_id -current_user_id $rest_user_id $category_id}]]

    set return_objects [im_rest_objects -sql $sql -rest_user_id $rest_user_id -valid_vars $valid_vars -data_type_arr data_types -additional_var_procs $additional_var_procs -format $format -rest_oid $rest_oid -rest_otype $rest_otype]

    switch $format {
        html {
            set page_title "object_type: $rest_otype"
            im_rest_doc_return 200 "text/html" "
            [im_header $page_title [im_rest_header_extra_stuff]][im_navbar]<table>
            <tr class=rowtitle><td class=rowtitle>object_id</td><td class=rowtitle>Link</td></tr>$return_objects
            </table>[im_footer]
            " 
        }
        json {
            set json_list $return_objects
            set result "{\"success\": true,\n\"total\": [llength $json_list],\n\"message\": \"im_rest_get_object_type: Data loaded\",\n\"data\": \[\n[join $json_list ","]\n\]\n}"
            im_rest_doc_return 200 "application/json" $result
        }
        default {
            ad_return_complaint 1 "im_rest_get_object_type: Invalid format5: '$format'"
        }
    }
}


ad_proc -private im_rest_get_im_dynfield_attribute {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest calls on dynfield attributes
} {
    ns_log Notice "im_rest_get_im_dynfield_attribute: format=$format, rest_user_id=$rest_user_id, rest_otype=$rest_otype, query_hash=$query_hash_pairs"
    array set query_hash $query_hash_pairs
    set base_url "[im_rest_system_url]/intranet-rest"
    if {"" != $rest_oid} { set query_hash(attribute_id) $rest_oid }
    
    set rest_otype_id [util_memoize [list db_string otype_id "select object_type_id from im_rest_object_types where object_type = 'im_dynfield_attribute'" -default 0]]
    set rest_otype_read_all_p [im_object_permission -object_id $rest_otype_id -user_id $rest_user_id -privilege "read"]

    set deref_p 0
    if {[info exists query_hash(deref_p)]} { set deref_p $query_hash(deref_p) }
    im_security_alert_check_integer -location "im_rest_get_im_dynfield_attribute: deref_p" -value $deref_p
    
    # -------------------------------------------------------
    # Check if there is a where clause specified in the URL and validate the clause.
    set where_clause ""
    if {[info exists query_hash(query)]} { set where_clause $query_hash(query)}
    # Determine the list of valid columns for the object type
    set valid_vars [util_memoize [list im_rest_object_type_columns -deref_p $deref_p -rest_otype $rest_otype]]
    set valid_vars [concat $valid_vars {object_type table_name attribute_name pretty_name pretty_plural datatype default_value min_n_values max_n_values storage static_p column_name}]
   
    # Check that the query is a valid SQL where clause
    set valid_sql_where [im_rest_valid_sql -string $where_clause -variables $valid_vars]
    if {!$valid_sql_where} {
	return [im_rest_error -format $format -http_status 403 -message "The specified query is not a valid SQL where clause: '$where_clause'"]
    }
    if {"" != $where_clause} { set where_clause "and $where_clause" }

    # Select SQL: Pull out values.
    set sql "
	select	
		aa.object_type||'.'||aa.attribute_name as rest_object_name,
		da.attribute_id as rest_oid,
		da.*,
		aa.*
	from	im_dynfield_attributes da,
		acs_attributes aa
	where	da.acs_attribute_id = aa.attribute_id
		$where_clause
	order by
		aa.object_type, 
		aa.attribute_name
    "

    # Append pagination "LIMIT $limit OFFSET $start" to the sql.
    set unlimited_sql $sql
    append sql [im_rest_object_type_pagination_sql -query_hash_pairs $query_hash_pairs]


    set result ""
    set obj_ctr 0
    db_foreach objects $sql {

	# Check permissions
	set read_p $rest_otype_read_all_p
	if {!$read_p} { continue }

	set url "$base_url/$rest_otype/$rest_oid"
	switch $format {
	    html { 
		append result "<tr>
			<td>$rest_oid</td>
			<td><a href=\"$url?format=html\">$rest_object_name</a>
		</tr>\n" 
	    }
	    json {
		set komma ",\n"
		if {0 == $obj_ctr} { set komma "" }
		set dereferenced_result ""
		foreach v $valid_vars {
			eval "set a $$v"
			regsub -all {\n} $a {\n} a
			regsub -all {\r} $a {} a
			append dereferenced_result ", \"$v\": \"[im_quotejson $a]\""
		}
		append result "$komma{\"id\": \"$rest_oid\", \"object_name\": \"[im_quotejson $rest_object_name]\"$dereferenced_result}" 
	    }
	    default {}
	}
	incr obj_ctr
    }
	
    switch $format {
	html { 
	    set page_title "object_type: $rest_otype"
	    im_rest_doc_return 200 "text/html" "
		[im_header $page_title [im_rest_header_extra_stuff]][im_navbar]<table>
		<tr class=rowtitle><td class=rowtitle>object_id</td><td class=rowtitle>Link</td></tr>$result
		</table>[im_footer]
	    " 
	}
	json {
	    set result "{\"success\": true,\n\"total\": $obj_ctr,\n\"message\": \"im_rest_get_im_dynfield_attribute: Data loaded\",\n\"data\": \[\n$result\n\]\n}"
	    im_rest_doc_return 200 "application/json" $result
	    return
	}
    }

    return
}


ad_proc -private im_rest_get_im_indicator_result {
    { -format "xml" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -query_hash_pairs {} }
    { -rest_oid ""}
    { -debug 0 }
} {
        Handler for GET rest calls on indicator results
} {
    
    # Note: var "result" had been replaced with "output" since it is used in table im_indicator_results
    ns_log Notice "im_rest_get_im_indicator_result: format=$format, user_id=$rest_user_id, rest_otype=$rest_otype, query_hash=$query_hash_pairs"
    array set query_hash $query_hash_pairs
        set base_url "[im_rest_system_url]/intranet-rest"
    
    set rest_indicator_otype_id [util_memoize [list db_string otype_id "select object_type_id from im_rest_object_types where object_type = 'im_indicator'" -default 0]]
    set rest_otype_read_all_p [im_object_permission -object_id $rest_indicator_otype_id -user_id $rest_user_id -privilege "read"]
    
    # Get locate for translation
    set locale [lang::user::locale -user_id $rest_user_id]
    
    # -------------------------------------------------------
    # Valid variables to return indicators
    set valid_vars {result_id result_indicator_id result_date result_date_pretty result result_count result_system_key result_sector_id result_company_size result_geo_region_id result_object_id}
    
    # -------------------------------------------------------
    # Check if there is a where clause specified in the URL and validate the clause.
    set where_clause ""
    if {[info exists query_hash(query)]} { set where_clause $query_hash(query)}
    
    # -------------------------------------------------------
    # Check if there are "valid_vars" specified in the HTTP header
    # and add these vars to the SQL clause
    set where_clause_list [list]
    foreach v $valid_vars {
	if {[info exists query_hash($v)]} { lappend where_clause_list "$v=$query_hash($v)" }
    }
    if {"" != $where_clause && [llength $where_clause_list] > 0} { append where_clause " and " }
    append where_clause [join $where_clause_list " and "]
    
    # Check that the query is a valid SQL where clause
    set valid_sql_where [im_rest_valid_sql -string $where_clause -variables $valid_vars]
    
    if {!$valid_sql_where} {
	return [im_rest_error -format $format -http_status 403 -message "The specified query is not a valid SQL where clause: '$where_clause'"]
    }
    if {"" != $where_clause} { set where_clause "and $where_clause" }
    
    # Single Object?
    set where_clause_oid ""
    if { "" != $rest_oid } { set where_clause_oid "and result_id = :rest_oid" }
    
    # Select SQL: Pull out categories.
    set sql "
                select
                        result_id as rest_oid,
                        to_char(r.result_date, 'YYYY-MM-DD') as result_date_pretty,
                        r.*
                from
                        im_indicator_results r
                where
                        1=1
                        $where_clause
                        $where_clause_oid
                order by
                        result_id
        "
    
    # Append pagination "LIMIT $limit OFFSET $start" to the sql
    
    set unlimited_sql $sql
    append sql [im_rest_object_type_pagination_sql -query_hash_pairs $query_hash_pairs]
    set value ""
    set output ""
    set obj_ctr 0
    
    db_foreach objects $sql {
	
        # Calculate indent
	# set indent [expr [string length tree_sortkey] - 8]
	
        # Check permissions
        set read_p $rest_otype_read_all_p
        set read_p 1
        if {!$read_p} { continue }
	
        set url "$base_url/$rest_otype/$rest_oid"
	
	switch $format {
	    xml { append output "<object_id id=\"$rest_oid\" href=\"$url\">$result_id</object_id>\n" }
	    html {
		append output "<tr>
                                <td>$rest_oid</td>
                                <td>$result_indicator_id</td>
                                        <td>$result_date_pretty</td>
                                        <td align='right'>$result</td>
                                        <td align='right'>$result_count</td>
                                        <td align='right'>$result_system_key</td>
                                        <td align='right'>$result_sector_id</td>
                                        <td align='right'>$result_company_size</td>
                                        <td align='right'>$result_geo_region_id</td>
                                        <td align='right'>$result_object_id</td>
                        </tr>\n"
	    }
	    json {
		set komma ",\n"
		if {0 == $obj_ctr} { set komma "" }
		set dereferenced_result ""
		foreach v $valid_vars {
		    eval "set a $$v"
		    regsub -all {\n} $a {\n} a
		    regsub -all {\r} $a {} a
		    append dereferenced_result ", \"$v\": \"[ns_quotehtml $a]\""
		}
		append output "$komma{\"id\": \"$rest_oid\", \"object_name\": \"[ns_quotehtml $result_id]\"$dereferenced_result}"
	    }
	    default {}
	}
	incr obj_ctr
    }
    
    switch $format {
	html {
	    set page_title "object_type: $rest_otype"
	    im_rest_doc_return 200 "text/html" "
                [im_header $page_title [im_rest_header_extra_stuff]][im_navbar]<table>
                <tr class=rowtitle>
                        <td class=rowtitle>object_id</td>
                        <td class=rowtitle>result_indicator_id</td>
                        <td class=rowtitle>date</td>
                        <td class=rowtitle>result</td>
                        <td class=rowtitle>result_count</td>
                        <td class=rowtitle>result_system_key</td>
                        <td class=rowtitle>result_sector_id</td>
                        <td class=rowtitle>result_company_size</td>
                        <td class=rowtitle>result_geo_region_id</td>
                        <td class=rowtitle>result_object_id</td>
                 </tr>
                         $output
                </table>[im_footer]
                "
	    return
	}
	xml {
	    im_rest_doc_return 200 "text/xml" "<?xml version='1.0'?>\n<object_list>\n$output</object_list>\n"
	    return
	}
	json {
	    # Deal with different JSON variants for different AJAX frameworks
	    set output "{\"success\": true,\n\"message\": \"im_rest_get_im_indicator_result: Data loaded\",\n\"data\": \[\n$output\n\]\n}"
	    im_rest_doc_return 200 "text/html" $output
	    return
	}
    }
    return
}


ad_proc -public im_rest_get_custom_key_value_tcl {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Provide a list of key_values (namely id and object_name) based on the TCL
    
    tcl is required
} {

    array set query_hash $query_hash_pairs
    
    if {[info exists query_hash(tcl_proc)]} {
        set tcl_proc $query_hash(tcl_proc)
    } else {
        # We can't work without a project!
        im_rest_error -format "json" -http_status 403 -message "Missing a tcl_proc to start with"
    }

    set valid_procs [list lang::system::get_locale_options im_country_options]
    
    if {[lsearch $valid_procs $tcl_proc]<0} {
        im_rest_error -format "json" -http_status 403 -message "$tcl_proc is not a procedure we support yet"
    }
    
    set obj_ctr 0
    set result ""
    foreach key_value_list [$tcl_proc] {
        set komma ",\n"
        if {0 == $obj_ctr} { set komma "" }
        append result "$komma{\"id\": \"[lindex $key_value_list 1]\", \"object_name\": \"[lindex $key_value_list 0]\"}"
        incr obj_ctr
    }
    
    set result "{\"success\": true,\n\"total\": $obj_ctr,\n\"message\": \"im_sencha_tables_rest_call: Data loaded\",\n\"data\": \[\n$result\n\]\n}"
    im_rest_doc_return 200 "application/json" $result
    return
}

ad_proc -public im_rest_get_custom_materials {
    -material_type_ids
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest calls which returns materials for given type_id (multiple comma seperated ids possible)

    @param material_type_id id of required material type

    @return material_id id of material
    @return material_name id of material
    @return material_nr id of material
    @return material_type_id id of material
    @return material_status_id
    @return description
    @return material_uom_id
    @return material_billable_p

} {
 
    set requested_materials_list [list]
    set success true
    set obj_ctr 0
    set error ""

    set requested_materials_ids ""
    foreach material_type_id $material_type_ids {
        lappend requested_materials_ids $material_type_id
    }
     
    if {[llength $requested_materials_ids] > 0} {

        set requested_materials_sql "select * from im_materials where material_type_id in ([ns_dbquotelist $requested_materials_ids])"

        db_foreach get_materials $requested_materials_sql {
            lappend requested_materials_list [im_rest_json_object -proc_name  im_rest_get_custom_materials]
        }

        set obj_ctr [llength $requested_materials_list]

        set requested_materials [join $requested_materials_list ","]
    }

    set result "{\"success\": $success, \"total\":$obj_ctr, \n\"data\": \[\n$requested_materials\n\]\n}"
    im_rest_doc_return 200 "application/json" $result
    return

}
