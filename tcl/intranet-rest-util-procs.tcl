# /packages/intranet-rest/tcl/intranet-rest-util-procs.tcl
#
# Copyright (C) 2009 ]project-open[
#
# All rights reserved. Please check
# http://www.project-open.com/license/ for details.

ad_library {
    REST Web Service Library
    Utility functions
    @author frank.bergmann@project-open.com
}

# --------------------------------------------------------
# Auxillary functions
# --------------------------------------------------------

ad_proc -public im_rest_doc_return {args} {
    This is a replacement for doc_return that values if the
    gzip_p URL parameters has been set.
} {
    # Perform some magic work
    db_release_unused_handles
    ad_http_cache_control

    set outputheaders [ns_conn outputheaders]
    ns_set cput  [ns_conn outputheaders] "access-control-allow-origin" "*"
    
	# find out if we should compress or not
    set query_set [ns_conn form]
    set gzip_p [ns_set get $query_set gzip_p]
    ns_log Debug "im_rest_doc_return: gzip_p=$gzip_p"

    # Return the data
    if {"1" == $gzip_p} {
	return [eval "ns_returnz $args"]
    } else {
	return [eval "ns_return $args"]
    }

}


ad_proc -public im_rest_get_rest_columns {
    query_hash_pairs
} {
    Reads the "columns" URL variable and returns the 
    list of selected REST columns or an empty list 
    if the variable was not specified.
} {
    set rest_columns [list]
    set rest_column_arg ""
    array set query_hash $query_hash_pairs
    if {[info exists query_hash(columns)]} { set rest_column_arg $query_hash(columns) }
    if {"" != $rest_column_arg} {
        # Accept both space (" ") and komma (",") separated columns
	set rest_columns [split $rest_column_arg " "]
	if {[llength $rest_columns] <= 1} {
	    set rest_columns [split $rest_column_arg ","]
	}
    }

    return $rest_columns
}


ad_proc -private im_rest_header_extra_stuff {
    {-debug 1}
} {
    Returns a number of HTML header code in order to make the 
    REST interface create reasonable HTML pages.
} {
    set extra_stuff "
	<link rel='stylesheet' href='/resources/acs-subsite/default-master.css' type='text/css' media='all'>
	<link rel='stylesheet' href='/intranet/style/style.saltnpepper.css' type='text/css' media='screen'>
	<link rel='stylesheet' href='/resources/acs-developer-support/acs-developer-support.css' type='text/css' media='all'> 
	<link rel='stylesheet' href='/intranet/style/smartmenus/sm-core-css.css'  type='text/css' media='screen'>
	<link rel='stylesheet' href='/intranet/style/smartmenus/sm-simple/sm-simple.css'  type='text/css' media='screen'>
	<script type='text/javascript' src='/intranet/js/jquery.min.js'></script>
	<script type='text/javascript' src='/intranet/js/showhide.js'></script>
	<script type='text/javascript' src='/intranet/js/rounded_corners.inc.js'></script>
	<script type='text/javascript' src='/resources/acs-subsite/core.js'></script>
	<script type='text/javascript' src='/intranet/js/smartmenus/jquery.smartmenus.min.js'></script>
	<script type='text/javascript' src='/intranet/js/style.saltnpepper.js'></script>
    "
}


ad_proc -private im_rest_debug_headers {
    {-debug 1}
} {
    Show REST call headers
} {
    set debug "\n"
    append debug "method: [ns_conn method]\n"
    
    set header_vars [ns_conn headers]
    foreach var [ad_ns_set_keys $header_vars] {
	set value [ns_set get $header_vars $var]
	append debug "header: $var=$value\n"
    }
    
    set form_vars [ns_conn form]
    foreach var [ad_ns_set_keys $form_vars] {
	set value [ns_set get $form_vars $var]
	append debug "form: $var=$value\n"
    }
    
    append debug "content: [ns_conn content]\n"
    
    ns_log Debug "im_rest_debug_headers: $debug"
    return $debug
}


ad_proc -private im_rest_system_url { } {
    Returns a the system's "official" URL without trailing slash
    suitable to prefix all hrefs used for the JSON format.
} {
    return [util_current_location]
}


# ----------------------------------------------------------------------
# Extract all fields from an object type's tables
# ----------------------------------------------------------------------

ad_proc -public im_rest_object_type_pagination_sql { 
    { -query_hash_pairs "" }
	{ -limit "" }
	{ -start "" }
} {
    Appends pagination information to a SQL statement depending on
    URL parameters: "LIMIT $limit OFFSET $start".
} {
    set pagination_sql ""
	if {$limit eq "" && $start eq ""} {
    	array set query_hash $query_hash_pairs

    	if {[info exists query_hash(limit)]} { 
			set limit $query_hash(limit) 
    	} else {
			set limit ""
		}

	    if {[info exists query_hash(start)]} { 
			set start $query_hash(start) 
    	} else {
			set start ""
		}
	}

	if {$limit ne ""} {
		im_security_alert_check_integer -location "im_rest_get_object_type" -value $limit
		append pagination_sql "LIMIT $limit\n"
	}

	if {$start ne ""} {
		im_security_alert_check_integer -location "im_rest_get_object_type" -value $start
		append pagination_sql "OFFSET $start\n"
	}

    return $pagination_sql
}

ad_proc -public im_rest_object_type_order_sql { 
    -query_hash_pairs:required
} {
    returns an "ORDER BY" statement for the *_get_object_type SQL.
    URL parameter example: sort=[{"property":"creation_date", "direction":"DESC"}]
} {
    set order_sql ""
    array set query_hash $query_hash_pairs

    set order_by_clauses {}
    if {[info exists query_hash(sort)]} { 
	set sort_json $query_hash(sort)
	array set parsed_json [util::json::parse $sort_json]
	set json_list $parsed_json(_array_)

	foreach sorter $json_list {
	    # Skpe the leading "_object_" key
	    set sorter_list [lindex $sorter 1]
	    array set sorter_hash $sorter_list

	    set property $sorter_hash(property)
	    set direction [string toupper $sorter_hash(direction)]
	    
	    # Perform security checks on the sorters
	    if {![regexp {} $property match]} { 
		ns_log Error "im_rest_object_type_order_sql: Found invalid sort property='$property'"
		continue 
	    }
	    if {[lsearch {DESC ASC} $direction] < 0} { 
		ns_log Error "im_rest_object_type_order_sql: Found invalid sort direction='$direction'"
		continue 
	    }
	    
	    lappend order_by_clauses "$property $direction"
	}
    }

    if {"" != $order_by_clauses} {
	return "order by [join $order_by_clauses ", "]\n"
    } else {
	# No order by clause specified
	return ""
    }
}

# ---------------------------------------------------------------
# Get meta-informatoin information about columns
#
# The deref_plpgsql_function is able to transform an attribute
# reference (i.e. an object_id or a category_id) into the name
# of the object.
# ---------------------------------------------------------------

ad_proc -public im_rest_hard_coded_deref_plpgsql_functions { 
    -rest_otype:required
} {
    Returns a key-value list of hard coded attribues per object type.
    These values are only necessary in order to work around missing
    dynfield metadata information for certain object types
} {
    set list {
	"acs_objects-creation_user" im_name_from_id
	"im_projects-parent_id" im_name_from_id
	"im_projects-company_id" im_name_from_id
	"im_projects-project_type_id" im_category_from_id 
	"im_projects-project_status_id" im_category_from_id 
	"im_projects-billing_type_id" im_category_from_id 
	"im_projects-on_track_status_id" im_category_from_id 
	"im_projects-project_lead_id" im_name_from_id 
	"im_projects-supervisor_id" im_name_from_id 
	"im_projects-company_contact_id" im_name_from_id 
	"im_projects-project_cost_center_id" im_name_from_id 
	"im_conf_items-conf_item_parent_id" im_name_from_id
	"im_conf_items-conf_item_cost_center_id" im_name_from_id
	"im_conf_items-conf_item_owner_id" im_name_from_id
	"im_conf_items-conf_item_type_id" im_name_from_id
	"im_conf_items-conf_item_status_id" im_name_from_id
    }
    return $list
}

ad_proc -public im_rest_deref_plpgsql_functions { 
    -rest_otype:required
} {
    Returns a key-value list of dereference functions per table-column.
} {
    set dynfield_sql "
    	select	*
	from	acs_attributes aa,
		im_dynfield_attributes da,
		im_dynfield_widgets dw
	where	aa.attribute_id = da.acs_attribute_id and
		da.widget_name = dw.widget_name and
		aa.object_type = :rest_otype
    "
    # Get a list of hard-coded attributes
    array set dynfield_hash [im_rest_hard_coded_deref_plpgsql_functions -rest_otype $rest_otype]
    # Overwrite/add with list of meta information from DynFields
    db_foreach dynfields $dynfield_sql {
	set key "$table_name-$attribute_name"
	set dynfield_hash($key) $deref_plpgsql_function
    }

    return [array get dynfield_hash]
}


ad_proc -public im_rest_object_type_select_sql { 
    {-deref_p 0}
    {-no_where_clause_p 0}
    -rest_otype:required
} {
    Calculates the SQL statement to extract the value for an object
    of the given rest_otype. The SQL will contains a ":rest_oid"
    colon-variables, so the variable "rest_oid" must be defined in 
    the context where this statement is to be executed.
} {
    # get the list of super-types for rest_otype, including rest_otype
    # and remove "acs_object" from the list
    set super_types [im_object_super_types -object_type $rest_otype]
    set s [list]
    foreach t $super_types {
	if {$t eq "acs_object"} { continue }
	lappend s $t
    }
    set super_types $s

    # Get a list of dereferencing functions
    if {$deref_p} {
	array set dynfield_hash [im_rest_deref_plpgsql_functions -rest_otype $rest_otype]
    }

    # ---------------------------------------------------------------
    # Construct a SQL that pulls out all information about one object
    # Start with the core object tables, so that all important fields
    # are available in the query, even if there are duplicates.
    #
    set letters {a b c d e f g h i j k l m n o p q r s t u v w x y z}
    set from {}
    set froms {}
    set selects { "1 as one" }
    set selected_columns {}
    set selected_tables {}

    set tables_sql "
	select	table_name,
		id_column
	from	(
		select	table_name,
			id_column,
			1 as sort_order
		from	acs_object_types
		where	object_type in ('[join $super_types "', '"]')
		UNION
		select	table_name,
			id_column,
			2 as sort_order
		from	acs_object_type_tables
		where	object_type in ('[join $super_types "', '"]')
		) t
	order by t.sort_order
    "
    set table_list [db_list_of_lists tables $tables_sql]

    set cnt 0
    foreach table_tuple $table_list {
	set table_name [lindex $table_tuple 0]
	set id_column [lindex $table_tuple 1]

	# Make sure not to include a table twice! There are duplicates in the query.
	if {[lsearch $selected_tables $table_name] >= 0} { continue }

	# Define an abbreviation for each table
	set letter [lindex $letters $cnt]
	lappend froms "LEFT OUTER JOIN $table_name $letter ON (o.object_id = $letter.$id_column)"

	# Iterate through table columns
	set columns_sql "
		select	lower(column_name) as column_name
		from	user_tab_columns
		where	lower(table_name) = lower(:table_name)
	"
	db_foreach columns $columns_sql {
	    if {[lsearch $selected_columns $column_name] >= 0} { 
		ns_log Error "im_rest_object_type_select_sql: found ambiguous field: $table_name.$column_name"
		continue 
	    }
	    lappend selects "$letter.$column_name"
	    lappend selected_columns $column_name

	    # Check for dereferencing function
	    set key [string tolower "$table_name-$column_name"]
	    if {[info exists dynfield_hash($key)]} {
		set deref_function $dynfield_hash($key)
		lappend selects "${deref_function}($letter.$column_name) as ${column_name}_deref"
		lappend selected_columns ${column_name}_deref
	    }
	}

	lappend selected_tables $table_name
	incr cnt
    }

    set acs_object_deref_sql "im_name_from_user_id(o.creation_user) as creation_user_deref,"
    if {!$deref_p} { set acs_object_deref_sql "" }

    set sql "
	select	o.*,
		o.object_id as rest_oid,
		$acs_object_deref_sql
		acs_object__name(o.object_id) as object_name,
		[join $selects ",\n\t\t"]
	from	acs_objects o
		[join $froms "\n\t\t"]
    "
    if {!$no_where_clause_p} { append sql "
	where	o.object_id = :rest_oid
    "}

    return $sql
}


ad_proc -public im_rest_object_type_columns { 
    {-deref_p 0}
    {-include_acs_objects_p 1}
    -rest_otype:required
	{-data_type_arr ""}
} {
    Returns a list of all columns for a given object type.
} {
    set super_types [im_object_super_types -object_type $rest_otype]
    if {!$include_acs_objects_p} {
		# Exclude base tables if not necessary
		set super_types [lsearch -inline -all -not -exact $super_types acs_object]
		set super_types [lsearch -inline -all -not -exact $super_types im_biz_object]
    }

    # Get a list of dereferencing functions
    if {$deref_p} {
		array set dynfield_hash [im_rest_deref_plpgsql_functions -rest_otype $rest_otype]
    }

    # ---------------------------------------------------------------
    # Construct a SQL that pulls out all tables for an object type,
    # plus all table columns via user_tab_colums.
    set columns_sql "
	select distinct
		lower(utc.column_name) as column_name,
		lower(utc.table_name) as table_name,
		lower(utc.data_type) as data_type
	from
		user_tab_columns utc
	where
		(-- check the main tables for all object types
		lower(utc.table_name) in (
			select	lower(table_name)
			from	acs_object_types
			where	object_type in ('[join $super_types "', '"]')
		) OR
		-- check the extension tables for all object types
		lower(utc.table_name) in (
			select	lower(table_name)
			from	acs_object_type_tables
			where	object_type in ('[join $super_types "', '"]')
		)) and
		-- avoid returning 'format' because format=json is part of every query
		lower(utc.column_name) not in ('format', 'rule_engine_old_value')
    "

    set columns [list]
	if {$data_type_arr ne ""} {
		upvar $data_type_arr type_arr
	}
    db_foreach columns $columns_sql {
		lappend columns $column_name
		# Handle the datatypes
		if {$data_type_arr ne ""} {
			# Append the data type to the array
			set type_arr($column_name) $data_type
		}

		# Add potential deref functions
		set key "$table_name-$column_name"
		if {[info exists dynfield_hash($key)]} {
		    lappend columns ${column_name}_deref
			if {$data_type_arr ne ""} {
				set type_arr(${column_name}_deref) varchar
			}
		}
    }
    return $columns
}



ad_proc -public im_rest_object_type_index_columns { 
    -rest_otype:required
} {
    Returns a list of all "index columns" for a given object type.
    The index columns are the primary key columns of the object
    types's tables. They will all contains the same object_id of
    the object.
} {
    # ---------------------------------------------------------------
    # Construct a SQL that pulls out all tables for an object type,
    # plus all table columns via user_tab_colums.
    set index_columns_sql "
	select	id_column
	from	acs_object_type_tables
	where	object_type = :rest_otype
    UNION
	select	id_column
	from	acs_object_types
	where	object_type = :rest_otype
    UNION
	select	'rest_oid'
    "

    return [db_list index_columns $index_columns_sql]
}




ad_proc -public im_rest_object_type_subtypes { 
    -rest_otype:required
} {
    Returns a list of all object types equal or below
    rest_otype (including rest_otype).
} {
    set breach_p [im_security_alert_check_alphanum -location "im_rest_object_type_subtypes" -value $rest_otype]
    # Return a save value to calling procedure
    if {$breach_p} { return $rest_otype }

    set sub_type_sql "
	select	sub.object_type
	from	acs_object_types ot, 
		acs_object_types sub
	where	ot.object_type = '$rest_otype' and 
		sub.tree_sortkey between ot.tree_sortkey and tree_right(ot.tree_sortkey)
	order by sub.tree_sortkey
    "

    return [util_memoize [list db_list sub_types $sub_type_sql] 3600000]
}



# ----------------------------------------------------------------------
# Update all tables of an object type.
# ----------------------------------------------------------------------

ad_proc -public im_rest_object_type_update_sql { 
    { -format "json" }
    -rest_otype:required
    -rest_oid:required
    -hash_array:required
} {
    Updates all the object's tables with the information from the
    hash array.
} {
    ns_log Debug "im_rest_object_type_update_sql: format=$format, rest_otype=$rest_otype, rest_oid=$rest_oid, hash_array=$hash_array"

    # Stuff the list of variables into a hash
    array set hash $hash_array

    # ---------------------------------------------------------------
    # Get all relevant tables for the object type
    set tables_sql "
			select	table_name,
				id_column
			from	acs_object_types
			where	object_type = :rest_otype
		    UNION
			select	table_name,
				id_column
			from	acs_object_type_tables
			where	object_type = :rest_otype
    "
    db_foreach tables $tables_sql {
	set index_column($table_name) $id_column
	set index_column($id_column) $id_column
    }

    set columns_sql "
	select	lower(utc.column_name) as column_name,
		lower(utc.table_name) as table_name
	from
		user_tab_columns utc,
		($tables_sql) tables
	where
		lower(utc.table_name) = lower(tables.table_name)
	order by
		lower(utc.table_name),
		lower(utc.column_name)
    "

    array unset sql_hash
    db_foreach cols $columns_sql {

	# Skip variables that are not available in the var hash
	if {![info exists hash($column_name)]} { continue }

	# Skip index columns
	if {[info exists index_column($column_name)]} { continue }

	# skip tree_sortkey stuff
	if {"tree_sortkey" == $column_name} { continue }
	if {"max_child_sortkey" == $column_name} { continue }

	# ignore reserved variables
	if {"rest_otype" == $column_name} { contiue }
	if {"rest_oid" == $column_name} { contiue }
	if {"hash_array" == $column_name} { contiue }

	# ignore any "*_cache" variables (financial cache)
	if {[regexp {_cache$} $column_name match]} { continue }

	# Start putting together the SQL
	set sqls [list]
	if {[info exists sql_hash($table_name)]} { set sqls $sql_hash($table_name) }
	lappend sqls "$column_name = :$column_name"
	set sql_hash($table_name) $sqls
    }

    # Add the rest_oid to the hash
    set hash(rest_oid) $rest_oid

    ns_log Debug "im_rest_object_type_update_sql: [array get sql_hash]"

    foreach table [lsort [array names sql_hash]] {
	ns_log Notice "im_rest_object_type_update_sql: Going to update table '$table'"
	set sqls $sql_hash($table)
	set update_sql "update $table set [join $sqls ", "] where $index_column($table) = :rest_oid"

	if {[catch {
	    db_dml sql_$table $update_sql -bind [array get hash]
	} err_msg]} {
	    return [im_rest_error -format $format -http_status 404 -message "Error updating $rest_otype: '$err_msg'"]
	}
    }

    # Audit the action
    im_audit -action after_update -object_id $rest_oid

    ns_log Debug "im_rest_object_type_update_sql: returning"
    return
}



# ----------------------------------------------------------------------
# Error Handling
# ----------------------------------------------------------------------

ad_proc -public im_rest_error {
    { -http_status 404 }
    { -format "json" }
    { -message "" }
    -no_abort:boolean
} {
    Returns a suitable REST error message
} {

    set url [im_url_with_query]

    switch $http_status {
		200 { set status_message "OK: Success!" }
		304 { set status_message "Not Modified: There was no new data to return." }
		400 { 
			set status_message "Bad Request: The request was invalid. An accompanying error message will explain why." 
			ns_log Error "im_rest_error: $status_message - $message, http_status=$http_status, format=$format"
		}
		401 { set status_message "Not Authorized: Authentication credentials were missing or incorrect." }
		403 { 
			set status_message "Forbidden: The request is understood, but it has been refused.  An accompanying error message will explain why." 
			ns_log Error "im_rest_error: $status_message - $message, http_status=$http_status, format=$format"
		}
		404 { 
			set status_message "Not Found: The URI requested is invalid or the resource requested, for example a non-existing project." 
			ns_log Error "im_rest_error: $status_message - $message, http_status=$http_status, format=$format"
		}
		406 { 
			set status_message "Not Acceptable: Returned when an invalid format is specified in the request." 
			ns_log Error "im_rest_error: $status_message - $message, http_status=$http_status, format=$format"
		}
		500 { set status_message "Internal Server Error: Something is broken.  Please post to the &\#93;project-open&\#91; Open Discussions forum." }
		502 { set status_message "Bad Gateway: project-open is probably down." }
		503 { set status_message "Service Unavailable: project-open is up, but overloaded with requests. Try again later." }
		default { set status_message "Unknown http_status '$http_status'." }
    }

    set page_title [lindex [split $status_message ":"] 0]
    
    switch $format {
		html { 
			doc_return $http_status "text/html" "
			[im_header $page_title [im_rest_header_extra_stuff]][im_navbar]
			<p>$status_message</p>
			<pre>[ns_quotehtml $message]</pre>
			[im_footer]
			" 
		}
		json {  
			set result "{\"success\": false,\n\"message\": \"[im_quotejson $message]\"\n}"
			doc_return $http_status "application/json" $result
		}
		default {
			ad_return_complaint 1 "Invalid format1: '$format'"
		}
    }

    if {!$no_abort_p} {
		catch {ad_script_abort}
    }
    return 0
}

ad_proc ad_tmpnam {{template ""}} {
    A stub function to replace the deprecated "ns_tmpnam",
    which uses the deprecated C-library function "tmpnam()"
} {
    if {$template eq ""} {
        set template /tmp/oacs-XXXXXX
    }
    ns_mktemp $template
}

ad_proc -public im_rest_get_content {} {
    There's no [ns_conn content] so this is a hack to get the content of the REST request. 
    @return string - the request
    @author Dave Bauer
} {
    # (taken from aol30/modules/tcl/form.tcl)
    # Spool content into a temporary read/write file.
    # ns_openexcl can fail, since tmpnam is known not to
    # be thread/process safe.  Hence spin till success
    set fp ""
    while {$fp eq ""} {
        set filename "[ad_tmpnam][clock clicks -milliseconds].rpc2"
        set fp [ns_openexcl $filename]
    }

    fconfigure $fp -translation binary
    ns_conncptofp $fp
    close $fp

    set fp [open $filename r]
    while {![eof $fp]} {
        append text [read $fp]
    }
    close $fp
    file delete $filename
    # ns_unlink $filename     #; deprecated
    return $text
}

ad_proc -public im_rest_parse_json_content {
    { -format "" }
    { -content "" }
    { -rest_otype "" }
} {
    Parse the JSON content of a POST request with 
    the values of the object to create or update.
    @author Frank Bergmann
} {
    # Parse the HTTP content
    switch $format {
	json {
	    ns_log Debug "im_rest_parse_json_content: going to parse json content=$content"
	    # {"id":8799,"email":"bbigboss@tigerpond.com","first_names":"Ben","last_name":"Bigboss"}
	    array set parsed_json [util::json::parse $content]
	    set json_list $parsed_json(_object_)
	    array set hash_array $json_list

	    # ToDo: Modify the JSON Parser to return NULL values as "" (TCL NULL) instead of "null"
	    foreach var [array names hash_array] {
		set val $hash_array($var)
		if {"null" == $val} { set hash_array($var) "" }
	    }
	}
	default {
	    return [im_rest_error -http_status 406 -message "Unknown format: '$format'. Expected: {json}"]
	}
    }
    return [array get hash_array]
}

ad_proc -public im_rest_normalize_timestamp { date_string } {
    Reformat JavaScript date/timestamp format to suit PostgreSQL 8.4/9.x
    @author Frank Bergmann
} {
    set str $date_string

    # Cut off the GMT+0200... when using long format
    # Wed Jul 23 2014 19:23:26 GMT+0200 (Romance Daylight Time)
    if {[regexp {^(.*?)GMT\+} $str match val]} {
	set str $val
    }

    return $str
}


ad_proc -public im_quotejson { str } {
    Quote a JSON string. In particular this means escaping
    single and double quotes, as well as new lines, tabs etc.
    @author Frank Bergmann
} {
    regsub -all { \\ } $str { \\\\ } str
    regsub -all {"} $str {\"} str
    regsub -all {\n} $str {\\n} str
    regsub -all {\r} $str {\\r} str
    regsub -all {\t} $str {\\t} str


    set chars_to_be_escaped_list [list \
				      "\"" "\\\"" \\ \\\\ \b \\b \f \\f \n \\n \r \\r \t \\t \
				      \x00 \\u0000 \x01 \\u0001 \x02 \\u0002 \x03 \\u0003 \
				      \x04 \\u0004 \x05 \\u0005 \x06 \\u0006 \x07 \\u0007 \
				      \x0b \\u000b \x0e \\u000e \x0f \\u000f \x10 \\u0010 \
				      \x11 \\u0011 \x12 \\u0012 \x13 \\u0013 \x14 \\u0014 \
				      \x15 \\u0015 \x16 \\u0016 \x17 \\u0017 \x18 \\u0018 \
				      \x19 \\u0019 \x1a \\u001a \x1b \\u001b \x1c \\u001c \
				      \x1d \\u001d \x1e \\u001e \x1f \\u001f \x7f \\u007f \
				      \x80 \\u0080 \x81 \\u0081 \x82 \\u0082 \x83 \\u0083 \
				      \x84 \\u0084 \x85 \\u0085 \x86 \\u0086 \x87 \\u0087 \
				      \x88 \\u0088 \x89 \\u0089 \x8a \\u008a \x8b \\u008b \
				      \x8c \\u008c \x8d \\u008d \x8e \\u008e \x8f \\u008f \
				      \x90 \\u0090 \x91 \\u0091 \x92 \\u0092 \x93 \\u0093 \
				      \x94 \\u0094 \x95 \\u0095 \x96 \\u0096 \x97 \\u0097 \
				      \x98 \\u0098 \x99 \\u0099 \x9a \\u009a \x9b \\u009b \
				      \x9c \\u009c \x9d \\u009d \x9e \\u009e \x9f \\u009f \
				     ]

    set str [string map $chars_to_be_escaped_list $str]
    return $str
}

ad_proc -public im_rest_json_object {
    {-proc_name ""}
	{-object_class ""}
} {
    Returns a formated JSON line for the documented return json "Object"
    
    Uses the documented return parameters as variables which to call. Make sure they are
    set to the values you want before calling im_rest_json_element

	
	@author Malte Sussdorff (malte.sussdorff@cognovis.de)

	@param proc_name name of the procdure which is calling us. Not necessary though
	@param object_class name of the "class" of the object. Useful if you are working with nested objects (object within an object) or multiple object Lists

	@return Returns the formatted json string for the object based on the return_json documentation.
} {

	# Do uplevel magic to find out the procedure name
	if {$proc_name eq ""} {
		upvar proc_name_up proc_name_up
		uplevel {
			set proc_name_up [lindex [info level 0] 0]
		}
		set proc_name $proc_name_up
	}
	
    array set doc_elements [nsv_get api_proc_doc $proc_name]

	# Find if we have return elements which are actually an array
	set object_names [list]
	if { [info exists doc_elements(return)] } {
		foreach return_element $doc_elements(return) {
		    set return_items [split $return_element]
			set object_name [lindex $return_items 0]
			if { [info exists doc_elements(return_${object_name})]} {
				lappend object_names $object_name
			}
		}
	}

	foreach object_name $object_names {
		set value_json_strings [list]
		foreach return_element $doc_elements(return_${object_name}) {
			# The first item is supposed to be the variable
			set return_items [split $return_element]
			set v [lindex $return_items 0]
			set type [lindex $return_items 1]
			upvar $v a
			if {[info exists a]} {
				regsub -all {\n} $a {\n} a
				regsub -all {\r} $a {} a
				
				switch $type {
					number {
						# We might need to do a conversion integer vs. numeric....
						lappend value_json_strings "\"$v\": [im_quotejson $a]"
					}
					boolean {
						if {$string is true $a} {
							lappend value_json_strings "\"$v\": true"
						} else {
							lappend value_json_strings "\"$v\": false"							
						}
					}
					string {
						lappend value_json_strings "\"$v\": \"[im_quotejson $a]\""
					}
					object {
						# To eventually implement. create a new object out of the current variables
					}
					default {
						# Quote by default 
						lappend value_json_strings "\"$v\": \"[im_quotejson $a]\""
					}
				}
				
			} else {
				ns_log Error "im_rest_json_object for $proc_name can't reference $v"
				
				# Append the null value for non existing
				lappend value_json_strings "\"$v\": undefined"
			}
		}
		return "\{[join $value_json_strings ", "]\}"
    }
}

ad_proc -public im_rest_objects {
	-sql
	-valid_vars
	-data_type_arr 
	-rest_user_id
	{ -permission_proc "" }
	{ -format "json" }
	{ -rest_oid ""}
	{ -rest_otype ""}
	{ -additional_var_procs "" }
} {
	Returns a list of objects

	@param sql SQL Query to use for loading the data
	@param valid_vars List of valid vars to return
	@param permission_proc Procedure to check permissions with against each single line
	@param format format in which to return the json objects. might be html or json
	@param rest_oid Rest Object ID requested. Indicates we should only return a single object....
	@param rest_user_id UserID for this rest call.
	@param additional_var_procs List with potentially multiple variable_name & procedure to get it list. It is a list of var proc.
} {

	# Get the datatypes
	upvar $data_type_arr data_types

	# Store the result especially in case of HTML
	set result ""

	set json_list [list]
	set denied_objects [list]
	
	# Save keep the originally supplied rest_oid
	set org_rest_oid $rest_oid

    db_foreach objects $sql {
        # Skip objects with empty object name
        if {"" == $object_name} { 
            ns_log Error "im_rest_get_object_type: Skipping object #$rest_oid because object_name is empty."
            continue
        }

		# Permission_proc thing probably needs more debugging and testing.
		if {$permission_proc ne ""} {
          	ns_log Debug "im_rest_get_object_type: Checking for individual permissions: $permission_proc $rest_user_id $rest_oid"
           	eval "$permission_proc $rest_user_id $rest_oid view_p read_p write_p admin_p"

			# Apparently no read access... so continue
        	if {!$read_p} { 
				# Append the object_id to denied objects
				lappend denied_objects $rest_oid
				continue 
			}
		}

		# Set additional variables based on the proc
		if {$additional_var_procs ne ""} {
			foreach additional_var_proc $additional_var_procs {
				set variable_name [lindex $additional_var_proc 0]
				set procedure_to_exec [lindex $additional_var_proc 1]
				set $variable_name [eval $procedure_to_exec]
			}
		}
        switch $format {
            json {
                set json_object_list [list]
                set json_attributes ""
                foreach v $valid_vars {
                    set value [set $v]

                    # Correctly return NULL
                    if {$value eq ""} {
                        append json_attributes ", \"$v\": null"
                    } else {

                        # Check for the datatype of the var.
                        switch $data_types($v) {
                            int4 - int2 - float - number - float8 - numeric {
                                append json_attributes ", \"$v\": $value"
                            }
                            bpchar - boolean - bool {
                                if {[string is boolean $value]} {
                                    if {$value} {
                                        append json_attributes ", \"$v\": true"
                                    } else {
                                        append json_attributes ", \"$v\": false"
                                    }
                                } else {
                                    # Unsure what this is
                                    if {[string is double $value]} {
                                        # number
                                        append json_attributes ", \"$v\": $value"
                                    } else {
                                        append json_attributes ", \"$v\": \"[im_quotejson $value]\""
                                    }  
                                }
                            }
                            default {
                                # Treat everything as a string by default
                                append json_attributes ", \"$v\": \"[im_quotejson $value]\""
                            }
                        }
                    }
                }
                lappend json_list "{\"id\": $rest_oid, \"object_name\": \"[im_quotejson $object_name]\"$json_attributes}" 
            }
            html {
			    set base_url "[im_rest_system_url]/intranet-rest"
                set url "$base_url/$rest_otype/$rest_oid"
                append result "<tr>
                    <td>$rest_oid</td>
                    <td><a href=\"$url?format=html\">$object_name</a>
                </tr>\n" 
            }
        }
    }

	switch $format {
		html {
			return $result
		}
		json {
			if {$permission_proc ne "" && [llength $denied_objects]>0 && [llength $json_list] eq 0} {
				if {$org_rest_oid ne ""} {
					set message "User does not have access to object $org_rest_oid"
				} else {
					set message "User does not have access to objects of this type"
				}
				return [im_rest_error -format $format -http_status 403 -message $message -no_abort]
			} else {
				return $json_list
			}
		}
	}
}

ad_proc -public im_rest_json_object_from_list {
    -objects
} {
    Returns a formated JSON line for the documented return json "Object"
    
    Uses the passed objects to build json object
    
	@author Malte Sussdorff (malte.sussdorff@cognovis.de)

    @param objects list of objects which we want to turn into json
} {
    set final_json ""
    if {[llength objects]>0} {
	    set value_json_strings [list]
	    foreach single_object $objects {
	    	set value_json_strings ""
            array set single_obj_arr $single_object
            foreach single_obj_key [array names single_obj_arr] {
            	set obj_name $single_obj_key
            	set value $single_obj_arr($single_obj_key)
	            regsub -all {\n} $obj_name {\n} obj_name
	            regsub -all {\r} $obj_name {} $obj_name   
	            lappend value_json_strings "\"$obj_name\": \"[im_quotejson $value]\""  	
            }
            lappend final_json "\{[join $value_json_strings ", "]\}"
	    }    
	    return "[join $final_json ", "]"
    } else {
	    return ""
    }
}

ad_proc -public im_rest_proc_documentation {
    {-format text/html}
    -proc_name:required
    {-source_p 0}
} {

	Generates formatted documentation for a procedure.

	@author Malte Sussdorff (malte.sussdorff@cognovis.de)

	@param format the type of documentation to generate. Currently, only
				<code>text/html</code> and <code>openapi</code> are supported.
	@param proc_name Name of the procedure to return the documentation for
	
	@return the formatted documentation string.
	@error if the procedure is not defined.
} {

	if { $format ne "text/html" && $format ne "openapi" } {
		return -code error "Only text/html and text/plain documentation are currently supported"
	}
	
	array set doc_elements [nsv_get api_proc_doc $proc_name]
	array set flags $doc_elements(flags)
	array set default_values $doc_elements(default_values)

	set label $proc_name
	set pretty_name [api_proc_pretty_name -label $label $proc_name]

	set first_line_tag "<h3>"
	set end_tag "</h3>"

	append out $first_line_tag$pretty_name$end_tag
	
	set xotcl 0
	set pretty_proc_name $proc_name

	lappend command_line $pretty_proc_name
	foreach switch $doc_elements(switches0) {
		if { [lsearch $flags($switch) "boolean"] >= 0} {
			lappend command_line "\[ -$switch \]"
		} elseif { [lsearch $flags($switch) "required"] >= 0 } {
			lappend command_line "-$switch <i>$switch</i>"
		} else {
			lappend command_line "\[ -$switch <i>$switch</i> \]"
		}
	}

	append out "[util_wrap_list $command_line]\n<blockquote>\n"
	if { $doc_elements(deprecated_p) } {
		append out "<b><i>Deprecated."
		if { $doc_elements(warn_p) } {
			append out " Invoking this procedure generates a warning."
		}
		append out "</i></b><p>\n"
	}

	append out "[lindex $doc_elements(main) 0]<p><dl>"

    if { [info exists doc_elements(param)] } {
		foreach param $doc_elements(param) {
		    set param_items [split $param]
			set params([lindex $param_items 0]) [lrange $param_items 1 end]
		}
    }
	
	append out "<dt><b>Switches:</b></dt><dd><dl>\n"
		foreach switch $doc_elements(switches0) {
		    switch $switch {
			format - rest_user_id - rest_otype - rest_oid - query_hash_pairs {
			    # do nothing
			}
			default {
			    append out "<dt><b>-$switch</b>"
			    if { [lsearch $flags($switch) "boolean"] >= 0 } {
				append out " (boolean)"
			    }
			    
			    if { [info exists default_values($switch)] && $default_values($switch) ne "" } {
				append out " (defaults to <code>\"$default_values($switch)\"</code>)"
			    }
			    
			    if { [lsearch $flags($switch) "required"] >= 0 } {
				append out " (required)"
			    } else {
				append out " (optional)"
			    }
			    append out "</dt>"
			    if { [info exists params($switch)] } {
				append out "<dd>$params($switch)</dd>"
			    }
			}
		    }
		append out "</dl></dd>\n"
	}

    if { [info exists doc_elements(return)] } {
	append out "</br><dt><b>Return:</b></dt>"
	foreach return_element $doc_elements(return) {
	    set return_items [split $return_element]
	    # Check if we have procedure name in there
	    set potential_proc_name [lindex $return_items 1]
	    # If second column is a procedure, we fetch all the return parameters from it
	    if {$potential_proc_name ne "" && [info procs $potential_proc_name] ne ""} {
	    	append out "<div style='margin-bottom:14px;'><dt><b>[lindex $return_items 0]</b> - [lrange $return_items 2 end]:</dt>\n"
            array set helper_proc_doc_elements [nsv_get api_proc_doc $potential_proc_name]
            if { [info exists helper_proc_doc_elements(return)] } {
                array set flags $helper_proc_doc_elements(flags)
                foreach helper_proc_return_element $helper_proc_doc_elements(return) {
                     set external_variable $helper_proc_return_element
                     set external_variable_return_items [split $external_variable]
                     append out "<dt style='margin-left:14px;'>[lindex $external_variable_return_items 0]</dt><dd>[lrange $external_variable_return_items 1 end]</dd>\n"
                }
                append out "</div>"
	        } 
	    } else {
	    	append out "<dt><b>[lindex $return_items 0]</b></dt><dd>[lrange $return_items 1 end]</dd>\n"
	    }
	}
    }
    
    if { [info exists doc_elements(error)] } {
	append out "<p><dt><b>Error:</b></dt><dd>[join $doc_elements(error) "<br>"]</dd>\n"
    }
    
    append out [::apidoc::format_common_elements doc_elements]
    if { $source_p } {
	if {[parameter::get_from_package_key  -package_key acs-api-browser  -parameter FancySourceFormattingP  -default 1]} {
	    append out "<p><dt><b>Source code:</b></dt><dd>
<pre>[::apidoc::tcl_to_html $proc_name]<pre>
</dd><p>\n"
	} else {
	    append out "<p/><dt><b>Source code:</b></dt><dd>
<pre>[ns_quotehtml [api_get_body $proc_name]]<pre>
</dd><p>\n"
        }
      }

		# No "see also" yet.

		append out "</dl></blockquote>"

		return $out
}

ad_proc -public im_rest_endpoints {
	-include_default
	{ -operation "" }
} {
	Return a list of endpoints or "paths" as per OpenAPI connotation

	@param include_default This will include any object_types which can be handled using im_rest_get_object_type
	@param operation Limit to those endpoints for which we have the operation. Defaults to any

	@return endpoints list_of_lists with each endpoint providing operation, result and proc_name
} {

	if {$operation eq ""} {
		set operations [list get post delete]
	} else {
		set operations [list $operation]
	}

	set endpoints [list]
	
	foreach operation $operations {
		foreach proc_name [info commands im_rest_${operation}_*] {
			switch $operation {
				post {
					# Handle custom overwrite procs
					set regexp [regexp "im_rest_post_custom_(.*)" $proc_name match result]
					if {$regexp eq 0} {
						set regexp [regexp "im_rest_post_object_type_(.*)" $proc_name match result]
					}
					if {$regexp eq 0} {
						set regexp [regexp "im_rest_post_object_(.*)" $proc_name match result]
					}
					if {$regexp eq 0} {
						regexp "im_rest_post_(.*)" $proc_name match result
					}
					# Exclude the object one.
					if {$result ne "object_type" && $result ne "object" && $result ne "type" && $result ne ""} {
						# And don't duplicate
						if {[lsearch $endpoints $result]<0} {
							lappend endpoints $result
						}
					}
				}
				default {
					# Handle custom overwrite procs
					if {![regexp "im_rest_${operation}_custom_(.*)" $proc_name match result]} {
						regexp "im_rest_${operation}_(.*)" $proc_name match result
					}
					# Exclude the object one.
					if {$result ne "object_type" && $result ne "object"} {
						# And don't duplicate
						if {[lsearch $endpoints $result]<0} {
							lappend endpoints $result
						}
					}
				}
			}
		}
	}
	return $endpoints
}

ad_proc -public im_rest_endpoint_proc {
	-endpoint:required
	-operation:required
} {
	Return the name of the procedure used for the endpoint

	@param endpoint name of the endpoint
	@param operation any of "GET" "POST" "DELETE"
} {
	set operation [string tolower $operation]

	set proc_name ""
	set valid_rest_otypes [util_memoize [list db_list otypes "
    	select	object_type 
		from	im_rest_object_types
    "]]

	switch $operation {
		get - delete {
			# Check if we have a command for the object type
			set proc_name "im_rest_${operation}_object_type"

			# Overwrite if we have a special object_type procedure
			if { [llength [info commands im_rest_${operation}_$endpoint]]} {
				set proc_name "im_rest_${operation}_$endpoint"
			} 

			# Override if we have a custom GET procedure
			if { [llength [info commands im_rest_${operation}_custom_$endpoint]]} {
				set proc_name "im_rest_${operation}_custom_$endpoint"
			}
			 
			if {$proc_name eq "im_rest_${operation}_object_type"} {
				# Check that the object_type is valid

    			if {[lsearch $valid_rest_otypes $endpoint] < 0} {
					set proc_name ""
				} 
			}
		}
		post {
			# Check if we have a command for the object type
			set proc_name "im_rest_post_object_type"

			if { [llength [info commands im_rest_post_object_$endpoint]]} {
				set proc_name "im_rest_${operation}_object_$endpoint"
			}

			# Overwrite if we have a special object_type procedure
			if { [llength [info commands im_rest_post_object_type_$endpoint]]} {
				set proc_name "im_rest_post_object_type_$endpoint"
			} 

			# Override if we have a custom GET procedure
			if { [llength [info commands im_rest_post_$endpoint]]} {
				set proc_name "im_rest_${operation}_custom_$endpoint"
			}


			if { [llength [info commands im_rest_post_custom_$endpoint]]} {
				set proc_name "im_rest_${operation}_custom_$endpoint"
			}

			if {$proc_name eq "im_rest_post_object_type"} {
				# Check that the object_type is valid

    			if {[lsearch $valid_rest_otypes $endpoint] < 0} {
					set proc_name ""
				} 
			}
		}
	}
	if {[llength [info commands $proc_name]]>0} {
		return $proc_name
	} else {
		return ""
	}
}

ad_proc -public im_rest_wiki_url {
	-object_type 
} {
	Returns the URL to the wiki for the object_type
	in Case we have one
} {
	# ]project-open[ wikis
	array set wiki_hash {
			object_type_im_indicator 1
			object_type_acs_attribute 1
			object_type_acs_object 1
			object_type_acs_object_type 1
			object_type_acs_permission 1
			object_type_acs_privilege 1
			object_type_acs_rel 1
			object_type_apm_package 1
			object_type_cal_item 1
			object_type_calendar 1
			object_type_group 1
			object_type_im_biz_object 1
			object_type_im_category 1
			object_type_im_company 1
			object_type_im_component_plugin 1
			object_type_im_conf_item 1
			object_type_im_cost 1
			object_type_im_cost_center 1
			object_type_im_dynfield_attribute 1
			object_type_im_employee 1
			object_type_im_expense 1
			object_type_im_expense_bundle 1
			object_type_im_forum_topic 1
			object_type_im_forum_topic_name 1
			object_type_im_fs_file 1
			object_type_im_hour 1
			object_type_im_indicator 1
			object_type_im_invoice 1
			object_type_im_invoice_item 1
			object_type_im_material 1
			object_type_im_menu 1
			object_type_im_office 1
			object_type_im_payment 1
			object_type_im_profile 1
			object_type_im_project 1
			object_type_im_report 1
			object_type_im_ticket 1
			object_type_im_ticket_ticket_rel 1
			object_type_im_timesheet_invoice 1
			object_type_im_timesheet_price 1
			object_type_im_timesheet_task 1
			object_type_im_user_absence 1
			object_type_im_view 1
			object_type_object 1
			object_type_party 1
			object_type_person 1
			object_type_user 1
		}
	
	set wiki_key "object_type_$object_type"
	regsub -all {_} $object_type {-} object_type_dashes
	if {[info exists wiki_hash($wiki_key)]} {
		set object_wiki_url "http://www.project-open.com/en/object-type-$object_type_dashes"
	} else {
		set object_wiki_url ""
	}
}

ad_proc -public im_rest_convert_to_yaml_string {
	-string_from:required
} {
	Returns a cleaned version of the string for inclusion in YAML code 

	@param string String to be converted
} {
    set string_to [util_convert_line_breaks_to_html $string_from]
    regsub -all {\n} $string_to {} string_to
    regsub -all {'} $string_to {''} string_to
	regsub -all {\{} $string_to {} string_to
	regsub -all {\}} $string_to {} string_to
	regsub -all {\[} $string_to {} string_to
	regsub -all {\]} $string_to {} string_to
	return [im_quotejson $string_to]
}