<master>
<property name=title>@title;noquote@</property>
<property name="context">@context;noquote@</property>

<table width="100%">
  <tr><td bgcolor="#eeeeee">@documentation;noquote@</td></tr>
</table>

<if @source_p@ eq 0>
[ <a href="endpoint-view?format=html&amp;proc_name=@proc_name@&amp;source_p=1">show source</a> ]
</if>
<else>
[ <a href="endpoint-view?format=html&amp;proc_name=@proc_name@&amp;source_p=0">hide source</a> ]
</else>
