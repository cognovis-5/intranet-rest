set format "html"

set title "Endpoints"

set context [list]
lappend context [list "Endpoints"]

set return_url ""
set endpoints_html ""
set custom_procs [list]
set custom_hash [lsort [lindex $query_hash_pairs 1]]

set end_points [list]
foreach custom_proc $custom_hash {
    if {![string match "*_arg_parser" $custom_proc]} {
	    if {[string match "im_rest_get_custom_*" $custom_proc]} {
	    		set end_point [string range $custom_proc 19 end]
	    	} else {
	    		set end_point [string range $custom_proc 12 end]
	    	}
	    	set endpoint_proc($end_point) $custom_proc
	    	lappend end_points $end_point
	}
}

foreach end_point [lsort $end_points] {
		set custom_proc $endpoint_proc($end_point)
	    	array set doc_elements [nsv_get api_proc_doc $custom_proc]
		array set flags $doc_elements(flags)
		array set default_values $doc_elements(default_values)
	
	    	set endpoint_url [export_vars -base "/intranet-rest/endpoint-view" -url {{proc_name $custom_proc}}]

	    	set out "<dt><b><a href=$endpoint_url>$end_point</a></b></dt><dd><dl>\n"
	    	set switch_exists_p 0
		foreach switch $doc_elements(switches0) {
			switch $switch {
				format - rest_user_id - rest_otype - rest_oid - query_hash_pairs - debug {
					# do nothing
				}
				default {
					set switch_exists_p 1
					append out "<dt><b>-$switch</b>"
					if { [lsearch $flags($switch) "boolean"] >= 0 } {
						append out " (boolean)"
					}
				
					if { [info exists default_values($switch)] && $default_values($switch) ne "" } {
						append out " (defaults to <code>\"$default_values($switch)\"</code>)"
					}
				
					if { [lsearch $flags($switch) "required"] >= 0 } {
						append out " (required)"
					} else {
						append out " (optional)"
					}
					append out "</dt>"
					if { [info exists params($switch)] } {
						append out "<dd>$params($switch)</dd>"
					}
				}
			}
		}
		
		if {!$switch_exists_p} {
			append out "<dt>Missing proper documentation in <a href=\"/api-doc/procs-file-view?path=[ns_urlencode $doc_elements(script)]\"> $doc_elements(script)</a>. Go and document me</dt>"
		}
		append out "</dl></dd>\n"
		append endpoints_html "$out"
}

# Create a list of all the procs for endpoints


switch $format {
    json {}
    default {}
}
