ad_page_contract {
    Display  OpenAPI Information of all documented API endpoints
    
    @cvs-id $Id$
} {
    { object_type "" }
}

set description "This is the overall endpoint documentation for **project-open** and all it''s endpoints"


#---------------------------------------------------------------
# Handle the paths
#---------------------------------------------------------------
set paths ""
set endpoints [im_rest_endpoints]

foreach endpoint $endpoints {

  if {[info exists doc_elements]} {
    array unset doc_elements
  }

  append paths "  /${endpoint}:\n"
  foreach operation [list get post] {
    set proc_name [im_rest_endpoint_proc -endpoint $endpoint -operation $operation]
    if {$proc_name ne ""} {
      # Okay... so we have a procedure which can handle this. 
      append paths "    ${operation}:\n"

      # Lets find out more
      # Get the documentation into an array first
      array set doc_elements [nsv_get api_proc_doc $proc_name]

      # Handle the flags to determine boolean / required
      array set flags $doc_elements(flags)
      array set default_values $doc_elements(default_values)

      # Get the summary from the summary... Or the object type
      if {[info exists doc_elements(summary)]} {
        set summary [im_rest_convert_to_yaml_string -string_from "[join $doc_elements(summary) "<br />"]"]
      } else {
        set summary [db_string object_type_pretty "select pretty_name from acs_object_types where object_type = :endpoint" -default ""]
      }
      if {$summary ne ""} {
        append paths "      summary: $summary\n"
      }

      # Find the wiki info
      set wiki_url [im_rest_wiki_url -object_type $endpoint] 
      if {$wiki_url ne ""} {
        append paths "      externalDocs:
        description: project-open Wiki Page
        url: $wiki_url \n"
      }

      # Get the details from the procedure
      switch $proc_name {
        im_rest_get_object_type - im_rest_post_object_type {
          # Generic object types
          set description "Using the generic handler for $endpoint . No specific information available.<br />You can use sort, limit, start"
          # Append the parameters

          # Dont use this for the moment as this requires the path to be modified. too much hassle for now"
          if {0} {
          append paths "      parameters:
      - name: rest_oid
        in: path
        description: Standard ID for the object of the type. will be correctly transformed
        schema:
          type: integer
          minItems: 0
          maxItems: 1\n"
          }
        }
        default {
          set description [im_rest_convert_to_yaml_string -string_from "[lindex $doc_elements(main) 0]"]
          append paths "      description: $description\n"
        }
      }

      # Use it here if we have switches....
      # append paths "      parameters:\n"
      set param_defined_switches [list]
      if {[info exists doc_elements(param)]} {
        append paths "      parameters:\n"

        foreach param $doc_elements(param) {
          set name [lindex $param 0]
          # Check if we have documented the type
          set type [lindex $param 1]

          switch $type {
            integer - string - boolean {
              set type $type
              set lrange_start 2
              set maxItems 1
            }
            json_string {
              set type "string"
              set lrange_start 2
              set maxItems ""
            }
            json_array {
              #set type "array"
              set type "string" ;# we use this for the time being as we have issues with correct array handling
              set lrange_start 2
              set maxItems ""
            }
            default {
              # No strict typing... assuming string..
              set type "string"
              set lrange_start 1
              set maxItems 1
              # Find out if we have an enum based on the dynfield widget associated with the acs_attribute
            }
          }

          # Find out if this is a required paramater
          if {[info exists flags($name)]} {
            # Search for required in the flags
            if {[lsearch $flags($name) "required"]<0} {
              set minItems 0
            } else {
              set minItems 1
            }
          }
          set description [im_rest_convert_to_yaml_string -string_from "[lrange $param $lrange_start end]"]
          append paths "      - name: $name
        in: query
        description: $description
        schema:
          type: $type
          minItems: $minItems\n"
          if {$maxItems ne ""} {
            append paths "          maxItems: 1\n"
          }

          # Append a default value if we have one
          if {[info exists default_values($name)] && $default_values($name) ne ""} {
            append paths "          default: $default_values($name)\n"
          }
          lappend param_defined_switches $name
        }
      }


      #---------------------------------------------------------------
      # Append the switches without @param description
      #---------------------------------------------------------------
      foreach switch $doc_elements(switches0) {

      }

      # Append the responses
      append paths "      responses:
        200:
          description: Successful response
          content:
            application/json:
              schema:
                title: testing
                type: object
                properties:
                  placeholder:
                    type: string
                    description: Placeholder description\n"
    }
  }
}

set header "openapi: '3.0.2'
info:
  title: '\]project-open\[ Endpoints'
  description: '$description'
  version: '1.0'
  contact:
    name: 'Malte Sussdorff'
    email: 'malte.sussdorff@cognovis.de'
  license:
    name: 'GPLv3'
    url: 'https://www.gnu.org/licenses/gpl-3.0.en.html'
paths:
$paths
servers:
  - url: [im_system_url]/intranet-rest/
    description: Current Server
  - url: https://kolibri.erp4projects.de/intranet-rest/
    description: Kolibri Staging
"

ns_return 200 "text/plain" $header